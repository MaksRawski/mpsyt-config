#!/usr/bin/python3
import json, os, pickle, sys

cfg="/home/"+os.popen("whoami").read()[:-1]+"/.config/mps-youtube/config"
def showHelp():
	print("""usage:  mpsyt-config.py [ -i --import ] config 
	mpsyt-config.py [ -e --extract ] filename (optional, config.json by default)
	""")
	exit(0)

if len(sys.argv)==1 or len(sys.argv)>3:
	showHelp()

#use config.json if no name is provided
if len(sys.argv)==2:
	fl="config.json"
else:
	fl=sys.argv[2]

if sys.argv[1]=="-e" or sys.argv[1]=="--extract":
	cf=open(cfg,"rb")
	config=pickle.load(cf)
	print(json.dumps(config,sort_keys=True, indent=4),file=open(fl,"w"))

elif sys.argv[1]=="-i" or sys.argv[1]=="--import":
	fil=json.load(open(fl,"rb"))
	pickle.dump(fil,open(cfg,"wb"),protocol=2)	

#TODO: make this part less crappy
elif sys.argv[1]=="music": 
	cf=open(cfg,"rb")
	config=pickle.load(cf)
	print(json.dumps(config,sort_keys=True, indent=4),file=open(fl,"w"))
	os.system("sed -i 's/SHOW_VIDEO.*/SHOW_VIDEO\": false,/' config.json")
	os.system("sed -i 's/SEARCH_MUSIC.*/SEARCH_MUSIC\": true,/' config.json")
	fil=json.load(open(fl,"rb"))
	pickle.dump(fil,open(cfg,"wb"),protocol=2)	

elif sys.argv[1]=="videos":
	cf=open(cfg,"rb")
	config=pickle.load(cf)
	print(json.dumps(config,sort_keys=True, indent=4),file=open(fl,"w"))
	os.system("sed -i 's/SHOW_VIDEO.*/SHOW_VIDEO\": true,/' config.json")
	os.system("sed -i 's/SEARCH_MUSIC.*/SEARCH_MUSIC\": false,/' config.json")
	fil=json.load(open(fl,"rb"))
	pickle.dump(fil,open(cfg,"wb"),protocol=2)	

else:
	showHelp()
